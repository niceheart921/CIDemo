package com.yuyue.share.sharedemo.bank;

/**
 * Created by fu on 16/4/18.
 */
public class BankDemo {

    /**
     * volatile 解决不了多线程抢资源的问题
     * synchronized 可以解决
     */
//    private volatile int count =0;//账户余额
    private int count =0;//账户余额

    //存钱
    public  void addMoney(int money){
        count +=money;
        System.out.println(System.currentTimeMillis()+"存进："+money);
    }

    //取钱
    public  void subMoney(int money){
        if(count-money < 0){
            System.out.println("余额不足");
            return;
        }
        count -=money;
        System.out.println(+System.currentTimeMillis()+"取出："+money);
    }

    //查询
    public void lookMoney(){
        System.out.println("账户余额："+count);
    }

}
