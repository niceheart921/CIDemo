package com.yuyue.share.sharedemo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by fu on 16/4/19.
 *
 * http://stackoverflow.com/questions/29290940/open-camera-for-input-type-file-in-webview-not-opening-android
 */
public class WebViewActivity extends Activity{
    private WebView webview = null;
    private String webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        webview = (WebView) findViewById(R.id.webView);

        webUrl = "http://kefu.easemob.com/webim/im.html?tenantId=10574&hide=false&sat=false&user=&referrer=";

//        initData();
        webview.getSettings().setJavaScriptEnabled(true); // 设置页面支持Javascript
//        webview.loadUrl(webUrl);


        //原生打开本地浏览器
//        Uri uri = Uri.parse(webUrl);
//        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        startActivity(intent);

//        webview.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                // TODO Auto-generated method stub
//                super.onPageFinished(view, url);
//            }
//
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                // TODO Auto-generated method stub
//                super.onPageStarted(view, url, favicon);
//            }
//        });

        webview.setWebChromeClient(new WebChromeClient(){
            // Need to accept permissions to use the camera
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.d("onPermissionRequest:","onPermissionRequest");
                request.grant(request.getResources());
            }
        });
        webview.loadUrl(webUrl);

        findViewById(R.id.btnQQ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url="mqqwpa://im/chat?chat_type=wpa&uin="+"393475141";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });

    }

    private void initData() {
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);//把加载优先级调到最高 2016/03/09 申鹏
        webview.getSettings().setBlockNetworkImage(true);//先让图片阻塞，不加载，把图片加载放在最后来加载渲染 2016/03/09 申鹏

        webview.getSettings().setJavaScriptEnabled(true); // 设置页面支持Javascript
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setSupportZoom(true); // 支持缩放
        webview.getSettings().setBuiltInZoomControls(true); // 显示放大缩小
//        webview.setInitialScale(130); // 初始化时缩放
        webview.getSettings().setDefaultTextEncodingName("utf-8");
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);// 解决缓存问题

        // 添加js的localStorage webview允许localStorage
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setAppCacheMaxSize(1024 * 1024 * 8);
        String appCachePath = getApplicationContext().getCacheDir()
                .getAbsolutePath();
        webview.getSettings().setAppCachePath(appCachePath);
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setAppCacheEnabled(true);

        //以下两句和硬件加速有关
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setAllowFileAccess(true);

        // webview.addJavascriptInterface(new JsInteration(), "callJS");
//        webview.setWebViewClient(new MyWebViewClient());
//        webview.loadUrl(webUrl);
//        webview.setWebChromeClient(new MyWebChromeClient(new WebChromeClient()));

//        webview.setWebViewClient(new WebViewClient(){
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                // TODO Auto-generated method stub
//                view.loadUrl(url);
//                return true;
//            }
//        });
        webview.loadUrl(webUrl);


    }

    /***
     * 自定义WebChromeClient，做选择图片处理
     * @author spring sky
     * 创建时间：Aug 19, 20133:40:46 PM
     */
    private class MyWebChromeClient extends WebChromeClient {


        // For Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
//            if (mUploadMessage != null) return;
//            mUploadMessage = uploadMsg;
//            selectImage();
        }
        // For Android < 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            openFileChooser( uploadMsg, "" );
        }
        // For Android  > 4.1.1
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
            openFileChooser(uploadMsg, acceptType);
        }

    }








}
