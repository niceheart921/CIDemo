package com.yuyue.share.sharedemo.gilde;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.module.GlideModule;

/**
 * glide加载高清图片
 *
 * @version V2.6 <描述当前版本功能>
 * @FileName: com.yuyuetech.frame.widget.glide.GlideConfiguration.java
 * @author: derek
 * @date: 2016-07-15 10:20
 */
public class GlideConfiguration implements GlideModule {

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDecodeFormat(DecodeFormat.PREFER_ARGB_8888);
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }
}
