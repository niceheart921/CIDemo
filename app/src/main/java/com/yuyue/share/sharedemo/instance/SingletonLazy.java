package com.yuyue.share.sharedemo.instance;

/**
 * java单例模式-懒加载（保证线程安全性）
 * Created by derek on 16/4/17.
 */
public class SingletonLazy {
    private static SingletonLazy instance = null;
    private SingletonLazy(){};
    public static  SingletonLazy getInstance(){
        if (null == instance){
            createInstance();
        }
        return instance;
    }

    private synchronized static SingletonLazy createInstance(){
        if (null == instance){
            instance = new SingletonLazy();
        }
        return instance;
    }

}
