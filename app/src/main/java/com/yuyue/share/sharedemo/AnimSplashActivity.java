package com.yuyue.share.sharedemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.yuyue.share.sharedemo.cache.ShareCache;


/**
 * 类用途:动画启动页
 *
 * @version V2.6 <描述当前版本功能>
 * @FileName: com.yuyuetech.yuyue.controller.account.AnimSplashActivity.java
 * @author: derek
 * @date: 2016-07-28 16:49
 */
public class AnimSplashActivity extends Activity {
    private static final String TAG = "AnimSplashActivity";
    private ImageView animImageView,animImageView1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim_splash);

        initData();

        if (!ShareCache.getInstance().getSharefEnv(AnimSplashActivity.this)){

        }

        System.out.println("赋值前Parent.baseUrl::" + Parent.baseUrl);
        System.out.println("赋值前Parent.baseUrl::" + Child.baseUrl);
        System.out.println("赋值前Child.newUrl::" + Child.newUrl);

        Parent.baseUrl = "www.taobao.com";
        System.out.println("赋值后Parent.baseUrl::"+Parent.baseUrl);
        System.out.println("赋值后Parent.baseUrl::"+Child.baseUrl);
        System.out.println("赋值后Child.newUrl::" + Child.newUrl);

        Child child = new Child();
        Child.baseUrl = "www.google.com.hk";
        System.out.println("Child赋值后Parent.baseUrl::"+Parent.baseUrl);
        System.out.println("Child赋值后Parent.baseUrl::"+Child.baseUrl);
        System.out.println("Child赋值后Child.newUrl::" + Child.newUrl);
        System.out.println("hild赋值后child.newUrl::" + Child.baseUrl + "?hahhaha");

        Child.updateUrl();
        System.out.println("Child updateUrl::" + Child.newUrl);



        animImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareCache.getInstance().saveSharefEnv(AnimSplashActivity.this, false);
                Parent.baseUrl = "www.jd.com";
                Child.updateUrl();
                System.out.println("Click赋值后Parent.baseUrl::"+Parent.baseUrl);
                System.out.println("Click赋值后Parent.baseUrl::" + Child.baseUrl);
                System.out.println("Click赋值后Child.newUrl::" + Child.newUrl);
                Intent mIntent = new Intent(AnimSplashActivity.this,AnimSplashActivity.class);
//                startActivity(mIntent);
            }
        });

    }

    private void initData() {
        animImageView = (ImageView) findViewById(R.id.animImageView);
        animImageView1 = (ImageView) findViewById(R.id.animImageView1);

        String imgId = "https://image.houpix.com/237211";
        String imgId1 = "https://image.houpix.com/237208";
        String imgId2 = "https://image.houpix.com/237210";

//        Glide.with(AnimSplashActivity.this).load(R.drawable.gif_anim_splash).into(animImageView);

        Glide.with(AnimSplashActivity.this)
                .load(imgId)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(animImageView);

        Glide.with(AnimSplashActivity.this)
                .load(imgId1)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(animImageView1);

    }
}
