package com.yuyue.share.sharedemo.cache;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * sharepreference 存储
 * Created by derek on 16/5/11.
 */
public class ShareCache {

    private static ShareCache mLoginType = null;

    private ShareCache() {
    };

    public synchronized static ShareCache getInstance() {
        if (mLoginType == null) {
            mLoginType = new ShareCache();
        }
        return mLoginType;
    }


    //快速登录
    private SharedPreferences sharedPreferences = null;
    public final static String SAVE_LOGIN_TYPE = "login_type_db";     //保存快速登录类型
    public final static String LOGIN_TYPE_KEY = "login_type_key";     //保存快速登录类型 key
    public final static String LOGIN_USERNAME_KEY = "login_username_key";     //用户名 key
    public final static String HEAD_IMAGE_ID_KEY = "login_head_image_id_key";     //头像id key
    public final static String JUDGE_ENV_KEY = "judge_env_key";     //判断环境


    /**
     * 保存登录类型
     * @param mActivity
     * @param loginType
     */
    public void saveSharefLoginType(Activity mActivity,String loginType) {
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_TYPE_KEY,loginType);
        editor.commit();
    }

    /**
     * 保存用户名和头像id
     * @param mActivity
     * @param userName
     * @param headImageId
     */
    public void saveSharefUserInfo(Activity mActivity,String userName,String headImageId) {
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_USERNAME_KEY,userName);
        editor.putString(HEAD_IMAGE_ID_KEY,headImageId);
        editor.commit();
    }

    public void saveSharefEnv(Activity mActivity,boolean env) {
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(JUDGE_ENV_KEY, env);
        editor.commit();
    }

    /**
     * 获取登录类型
     * @param mActivity
     * @return
     */
    public String getSharefLoginType(Activity mActivity) {
        String  loginType = "";
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        if (!TextUtils.isEmpty(sharedPreferences.getString(LOGIN_TYPE_KEY, ""))){
            loginType = sharedPreferences.getString(LOGIN_TYPE_KEY,"");
            return loginType;
        }
        return loginType;
    }

    /**
     * 获取用户名称
     * @param mActivity
     * @return
     */
    public String getSharefUserName(Activity mActivity) {
        String  userName = "";
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        if (!TextUtils.isEmpty(sharedPreferences.getString(LOGIN_USERNAME_KEY, ""))){
            userName = sharedPreferences.getString(LOGIN_USERNAME_KEY,"");
            return userName;
        }
        return userName;
    }

    /**
     * 获取头像id
     * @param mActivity
     * @return
     */
    public String getSharefHeadImageId(Activity mActivity) {
        String  imageId = "";
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        if (!TextUtils.isEmpty(sharedPreferences.getString(HEAD_IMAGE_ID_KEY, ""))){
            imageId = sharedPreferences.getString(HEAD_IMAGE_ID_KEY,"");
            return imageId;
        }
        return imageId;
    }


    public boolean getSharefEnv(Activity mActivity) {
        boolean  loginType = false;
        sharedPreferences = mActivity.getSharedPreferences(SAVE_LOGIN_TYPE, Context.MODE_PRIVATE);
        // 对数据进行编辑,返回的是一个Editor对象
        loginType = sharedPreferences.getBoolean(JUDGE_ENV_KEY, false);
        return loginType;
    }

}
