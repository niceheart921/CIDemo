package com.yuyue.share.sharedemo;

/**
 * 类用途
 *
 * @version V2.6 <描述当前版本功能>
 * @FileName: com.yuyue.share.sharedemo.Parent.java
 * @author: derek
 * @date: 2016-08-12 17:08
 */
public class Parent {
    private static final String TAG = "Parent";
    public static boolean ISDEBUG = true;

    public static String baseUrl = "";

    static {
        if (ISDEBUG){
            baseUrl = "test.baidu.com";
        }else{
            baseUrl = "www.baidu.com";
        }

    }

}
