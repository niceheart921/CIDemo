package com.yuyue.share.sharedemo;

import java.io.BufferedReader;
import java.io.FileReader;

public class Tests {

	public void test(Object o) {
		System.out.println("Object");
	}

	public void test(String s) {
		System.out.println("String");
	}
	public void test(int s) {
		System.out.println("String");
	}
	public void test(char s) {
		System.out.println("String");
	}

	static String s0, s1;
	static Object o;

	static String test3;

	public static void main(String[] args) {

//		saxHtml();

//		stringIntern();

		Tests that = new Tests();
		that.test(null);

		s0 = s0 + s1;
		System.out.println(s0);

		String a = "abc";
		String b = "ab" + "c";
		System.out.println(a == b);
		System.out.println(a.equals(b));

		System.out.println(o);

	}

	/**
	 * stringIntern 使用
	 */
	private static void stringIntern(){
		String s1 = new StringBuilder("软件").append("开发").toString();
		System.out.println(s1.intern() == s1);

		String s2 = new StringBuilder("ja").append("va").toString();
		System.out.println(s2.intern() == s2);

		String s3 = new StringBuilder("lo").append("ve").toString();
		System.out.println(s3.intern() == s3);
	}

	private static void objectValue(){
		String  n1 = "";
	}


	private static void saxHtml() {

		try {
			String str = "";
			String str1 = "";

			FileReader reader = new FileReader("/Users/fu/Desktop/icon/ux_1470971616_0550585/demo.html");
			BufferedReader bufferedReader = new BufferedReader(reader);
			for (int i = 0; i < 10000; i++) {

				str = bufferedReader.readLine();
				if (null != str) {
					if (str.contains("code")) {
						str1 = "\">\\ue" + str.substring(str.indexOf("#") + 3, str.indexOf("#") + 6) + "</string>";
					}
					if (str.contains("fontclass")) {
						str1 = " <string name=\"" + str.substring(str.indexOf(".") + 1, str.indexOf("/") - 1) + str1;
						System.out.println(str1);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
