package com.yuyue.share.sharedemo.bank;


import android.os.Handler;
import android.os.Message;

/**
 * Created by fu on 16/4/18.
 */
public class SyncThreadTest {
    private  static BankDemo bank = null;
    private static boolean isAdd = true;
    public static Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(isAdd){
                bank.addMoney(100);
                bank.lookMoney();
                System.out.println("\n");
            }else{
                bank.subMoney(100);
                bank.lookMoney();
                System.out.println("\n");
            }
        }
    };

    public static void main(String args[]){
        bank=new BankDemo();

        Thread tadd=new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while(true){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    isAdd = true;
                    mHandler.sendEmptyMessage(100);

                }
            }
        });

        Thread tsub = new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while(true){
                    isAdd = false;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    mHandler.sendEmptyMessage(100);
                }
            }
        });
        tsub.start();

        tadd.start();
    }

}
