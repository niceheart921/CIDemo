package com.yuyue.share.sharedemo;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeConfig;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.SinaShareContent;
import com.umeng.socialize.media.TencentWbShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.TencentWBSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.sso.UMSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;
import com.yuyue.share.sharedemo.contact.YuYueGlobalVariable;
import com.yuyue.share.sharedemo.largeview.LargeImageView;
import com.yuyue.share.sharedemo.widget.ShareDialog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,ShareDialog.ShareDialogListener{
    private TextView clickShare;

    private static UMSocialService mController;
    private Activity mActivity;
    private String mShareUrl = "http://baidu.com", shareContent = "yuyue", shareTitle = "share", mSharePic = "";// 分享URL
    private Bitmap shareBitmap = null;
    private String mTitle = "";
    private String mTitle1 = "";
    private String mTitle2 = "";
    private String mAddTitle = "feature add name";
    private String mAddTitle1 = "feature add name1";
    private String mAddImage = "feature add image";
    private ImageView imgBig = null;
    private Bitmap bitmap = null;
    private  String mImgUrl = "";
    private LargeImageView largeView = null;
    private  InputStream inStream = null;

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
           // imgBig.setImageBitmap(bitmap);
            largeView.setInputStream(inStream);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        mActivity = this;

        initData();

//        initYoumengShare();
    }

    private void initData() {
        clickShare = (TextView) findViewById(R.id.clickShare);
        clickShare.setOnClickListener(this);
//        imgBig = (ImageView)findViewById(R.id.imgBig);

        largeView = (LargeImageView) findViewById(R.id.largeView);

       // Glide.with(this).load("https://image.houpix.com/205430").fitCenter().into(imgBig);

        mImgUrl = "https://www.baidu.com/img/bd_logo1.png";
        mImgUrl = "https://image.houpix.com/205430";

        try {
           // InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("assets/"+"qm.jpg");
//            byte[] buf = getImageFromNetByUrl(mImgUrl);
//            InputStream inputStream = new ByteArrayInputStream(buf);
//            largeView.setInputStream(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                byte[] buf = getImageFromNetByUrl(mImgUrl);
                inStream = new ByteArrayInputStream(buf);
                mHandler.sendEmptyMessage(11);
            }
        }).start();


//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                bitmap = getNetWorkBitmap(mImgUrl);
//                mHandler.sendEmptyMessage(11);
//
//            }
//        }).start();

//        Glide.with(this).load(mImgUrl).into(imgBig);

       /* Glide.with(this)
                .load(mImgUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_launcher)
                .centerCrop()
                .signature(new StringSignature("60"))
                .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                        //正常加载图片调用
                        return false;
                    }
                })
                .into(imgBig);*/

    }

    private InputStream getImageStream(String strUrl){

        try {
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            inStream = conn.getInputStream();//通过输入流获取图片数据
            return inStream;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Bitmap getNetWorkBitmap(String urlString) {
        URL imgUrl = null;
        Bitmap bitmap = null;
        try {
            imgUrl = new URL(urlString);
            // 使用HttpURLConnection打开连接
            HttpURLConnection urlConn = (HttpURLConnection) imgUrl
                    .openConnection();
            urlConn.setDoInput(true);
            urlConn.connect();
            // 将得到的数据转化成InputStream
            InputStream is = urlConn.getInputStream();
            // 将InputStream转换成Bitmap
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            System.out.println("[getNetWorkBitmap->]MalformedURLException");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("[getNetWorkBitmap->]IOException");
            e.printStackTrace();
        }
        return bitmap;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.clickShare:
                shareWeb();
                break;
        }
    }

    /**
     * 分享入口
     *
     * @return
     */
    public void shareWeb() {
        ShareDialog sDialog = new ShareDialog(mActivity, MainActivity.this);
        sDialog.showDialog(0, 0);
    }

    private void initYoumengShare() {
        shareBitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);
        mController = UMServiceFactory.getUMSocialService("myshare");
        SocializeConfig config = SocializeConfig.getSocializeConfig();
        config.closeToast();
        mController.setGlobalConfig(config);
        initSocialSDK();

    }

    private void initSocialSDK() {

        // // 添加微信平台
        UMWXHandler wxHandler = new UMWXHandler(mActivity, YuYueGlobalVariable.APP_ID,
                YuYueGlobalVariable.APP_SECRET);
        wxHandler.addToSocialSDK();

        // 支持微信朋友圈
        UMWXHandler wxCircleHandler = new UMWXHandler(mActivity, YuYueGlobalVariable.APP_ID,
                YuYueGlobalVariable.APP_SECRET);
        wxCircleHandler.setToCircle(true);
        wxCircleHandler.addToSocialSDK();
        //QQ/空间
        UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler(mActivity, YuYueGlobalVariable.APP_ID_QQ,
                YuYueGlobalVariable.APP_KEY_QQ);
        qqSsoHandler.addToSocialSDK();

        // 设置新浪SSO handler
        mController.getConfig().setSsoHandler(new SinaSsoHandler());
        //腾讯微博
        mController.getConfig().setSsoHandler(new TencentWBSsoHandler());
    }


    private void shareEntry(SHARE_MEDIA platform, boolean isDirectShare) {

        // 设置文字分享内容
        mController.setShareContent(shareContent);
        // 图片分享内容   R.drawable.share_head
        mController.setShareMedia(new UMImage(mActivity,
                shareBitmap));
        if (platform == SHARE_MEDIA.WEIXIN) {
            // 设置微信好友分享内容
            WeiXinShareContent weixinContent = new WeiXinShareContent();

            // 设置分享文字
            weixinContent.setShareContent(shareContent);
            // 设置title
            weixinContent.setTitle(shareTitle);
            weixinContent.setShareImage(new UMImage(
                    mActivity, shareBitmap));
            // 设置分享内容跳转URL
            weixinContent.setTargetUrl(mShareUrl);
            mController.setShareMedia(weixinContent);

        } else if (platform == SHARE_MEDIA.WEIXIN_CIRCLE) {
            CircleShareContent circleMedia = new CircleShareContent();
            circleMedia.setShareContent(shareContent);
            // 设置朋友圈title
            circleMedia.setTitle(shareTitle);
            circleMedia.setTargetUrl(mShareUrl);
            circleMedia.setShareImage(new UMImage(
                    mActivity, shareBitmap));
            mController.setShareMedia(circleMedia);
        } else if (platform == SHARE_MEDIA.SINA) {

            SinaShareContent sinaContent = new SinaShareContent();
            sinaContent.setTargetUrl(mShareUrl);
        } else if (platform == SHARE_MEDIA.QQ) {
            // QQ写法必须如此才能保证优先级，不然分享出去会被友盟的官网覆盖
            QQShareContent qqContent = new QQShareContent();
            // 图片分享内容
            qqContent.setShareMedia(new UMImage(
                    mActivity, shareBitmap));
            qqContent.setTitle(shareTitle);
            qqContent.setShareContent(shareContent);
            qqContent.setTargetUrl(mShareUrl);
            mController.setShareMedia(qqContent);
        } else if (platform == SHARE_MEDIA.QZONE) {

            QZoneSsoHandler qZoneSsoHandler = new QZoneSsoHandler(
                    mActivity, "100424468",
                    "c7394704798a158208a74ab60104f0ba");
            qZoneSsoHandler.setTargetUrl(mShareUrl);
            qZoneSsoHandler.addToSocialSDK();

        } else if (platform == SHARE_MEDIA.TENCENT) {
            TencentWbShareContent txwbContent = new TencentWbShareContent();
            txwbContent.setTargetUrl(mShareUrl);
            txwbContent.setTitle(shareTitle);
        }

        if (isDirectShare) {
            // 调用直接分享
            mController.directShare(mActivity, platform,
                    mShareListener);
        } else {
            // 调用直接分享, 但是在分享前用户可以编辑要分享的内容
            mController.postShare(mActivity, platform,
                    mShareListener);
        }
    }

    /**
     * 分享监听器
     */
    SocializeListeners.SnsPostListener mShareListener = new SocializeListeners.SnsPostListener() {

        @Override
        public void onStart() {

        }

        @Override
        public void onComplete(SHARE_MEDIA platform, int stCode,
                               SocializeEntity entity) {
            if (stCode == 200) {
                Toast.makeText(mActivity, "分享成功",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "分享失败",
                        Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public void onClick(ShareDialog.ShareType type) {
        switch (type) {
            case WECHAT:
                shareEntry(SHARE_MEDIA.WEIXIN, true);
                // UMWXHandler wxHandler = new
                // UMWXHandler(mActivity, appId, appSecret);
                // wxHandler.addToSocialSDK();
                break;
            case WXCIRCLE:
                shareEntry(SHARE_MEDIA.WEIXIN_CIRCLE, true);
                break;
            case SINA:
                shareEntry(SHARE_MEDIA.SINA, true);
                break;
            case QQ:
                shareEntry(SHARE_MEDIA.QQ, true);
                break;
            case QZONE:
                shareEntry(SHARE_MEDIA.QZONE, true);
                break;
            case TXWB:
                shareEntry(SHARE_MEDIA.TENCENT, true);
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**使用SSO授权必须添加如下代码 */
        UMSsoHandler ssoHandler = mController.getConfig().getSsoHandler(requestCode) ;
        if(ssoHandler != null){
            ssoHandler.authorizeCallBack(requestCode, resultCode, data);

        }
    }

    /**
     * 根据地址获得数据的字节流
     * @param strUrl 网络连接地址
     * @return
     */
    public static byte[] getImageFromNetByUrl(String strUrl){
        try {
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            InputStream inStream = conn.getInputStream();//通过输入流获取图片数据
            byte[] btImg = readInputStream(inStream);//得到图片的二进制数据
            return btImg;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 从输入流中获取数据
     * @param inStream 输入流
     * @return
     * @throws Exception
     */
    public static byte[] readInputStream(InputStream inStream) throws Exception{
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while( (len=inStream.read(buffer)) != -1 ){
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }
}
