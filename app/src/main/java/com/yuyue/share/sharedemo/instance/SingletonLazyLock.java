package com.yuyue.share.sharedemo.instance;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * java单例模式-懒加载（保证线程安全性）lock
 * Created by derek on 16/4/17.
 */
public class SingletonLazyLock {
    private static SingletonLazyLock instance = null;
    private SingletonLazyLock(){};

    //加读写锁
    private static ReadWriteLock rwl = new ReentrantReadWriteLock();

    /**
     * 单例模式 懒加载并发
     * @return
     */
    public static SingletonLazyLock getInstance(){
        rwl.readLock().lock();
        try {
            if (null == instance){
                rwl.readLock().unlock();
                rwl.writeLock().lock();
                if(null == instance){
                    instance = new SingletonLazyLock();
                }
                rwl.writeLock().unlock();

            }
            rwl.readLock().lock();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rwl.readLock().unlock();
        }

        return instance;
    }


}
